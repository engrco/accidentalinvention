---
title: Agile Data Science 2.0    
subtitle: A One Book A Day Deep Dive
date: 2021-01-09
tags: ["intelligence", "genuine", "OBADDD", "TBD0"]
---


The rationale for looking into this kind of work is that we want to build useful tools that help us [and those who will use our stuff, ie dogfooding] search or network a lot better. We were searching for any and all ideas that further the goal of *agile*IFYING the explorative [end user] process in the browser, ie we want to be able to monkey with search engine parameters A LOT MORE than just the so-called "Advanced Search" options offered up ... we realize that probably means a ton of work curating our own data stores, APIs, newsfeeds, etc ... *agile* continues to be a very loaded word; process-ey stuff, eg SCRUM, tends to devolve into the opinionated noosystems of Wikipedia editing, so our *agile* is probably not going to be somebody else's *agile* -- but we are READY for the Two Oh portion of Data Science. 

So today one our OBAD (One Book a Day Deep Dive) we point our diver toward [Agile Data Science 2.0](https://learning.oreilly.com/library/view/agile-data-science/9781491960103) which is a serious book for beginners about building analytics applications with data of any size using Python and Spark ... it's not just for noobs monkeying around, but really people who want to get up to speed rapidly production teams where they will be collaborating on building, deploying, and refining analytics applications with Apache Kafka, MongoDB, ElasticSearch, d3.js, scikit-learn, and Apache Airflow in an agile manner ... as with ANY book, it is opinionated, but we find those opinions are entirely justified and reasonable -- for those seeking background, Russell Jurney's [Data Syndrome blog](https://blog.datasyndrome.com/) furnishes some additional insights into the author's thinking.

1. Theory
If you read just one chapter, this is the ONE. The Theory part should whet your appetite for the other step-by-step stuff that's going to be more hands-on [and you will understand and learn after getting your feet wet]. You NEED the Theory to help you to understand the relevance of the ideas presented, so that you can go back to really dive into the other parts when the day for implementation get closer. 

In a nutshell [but there's obviously more to it than just this paragraph], the [Theory chapter](https://learning.oreilly.com/library/view/agile-data-science/9781491960103/ch01.html) lays out how Agile Data Science implements or morphs the goals of intensely pragamatic, but code-driven [Agile Manifesto](http://agilemanifesto.org/) to the more uncertain world of data science ... this is something that can be perhaps something a paradox, since Agile Software [processes] does NOT make Agile Data Science [processes]. The GOALS might be similar, but data science is way, way more explorative.  It is necessary align results to needs of people who will use those results even when they won't know exactly what they are looking for when they start exploring ... it's not the same as when people with open up an app to do/get exactly what they think they want to get/do going in. It's necessarily exploratory. So Agile Data Science “goes meta” and focuses on the parts that are predictable, rather than the output itself, ie it's about the exploratory data analysis PROCESS not just the end state search results and documenting insight as it occurs.


2. Agile Tools

The [Tools chapter](https://learning.oreilly.com/library/view/agile-data-science/9781491960103/ch02.html) briefly introduces the software stack and how you might install the the stack for your own local laboratory environment ... the author does a superlative job of scope management and staying focused on the just-enough information is this chapter. Exploring any single one of the tools in the stack could go on for days or weeks ... and you, ultimately, are probably going to need to do that ... but there's just enough information to wet the old whistle, not enough to get full-blown drunk or go on a month long bender. It starts with getting the raw data from events/logs/data/traces/listening/observativability engineering sandboxes ... then onto Collectors like Kafta ... then Bulk Storage like Amazon S3 or Hadoop HDFS ... Batch Processing like Apache Spark ... Distributed Store like MongoDB ... Application Server like Flask ... and finally, there's a minimal discussion of the modern browser technologies which enable users to interact with your progressive web applications.

3. Data

The [Data chapter](https://learning.oreilly.com/library/view/agile-data-science/9781491960103/ch03.html) discusses the preferred starting point of semistructured data, using air travel data as working example for the book. This book briefly outlines how you would use the same kinds of tools that you would use at petabyte scale, but in local mode on your own machine, accordingly, this chapter is a VERY BRIEF outline of the perspectives selecting, obtaining and checking the data ... it's easy to gloss over a HUGE STEP like data selection and data wrangling, but you shouldn't -- obtaining the data and data wrangling is way more than half of worthwhile data science, the rest of it could be dismissed as playing with tools. The structure of the data going in exactly kind of really important stuff that people tend NOT really think about until they have moved forward with some sort of automated clusterfuck which is all a house of cards because it's built on clusterfucky kluge of junky data ... and ONLY THEN, do people get *really defensive* about their little science project ... so PLEASE read this chapter and think CRITICALLY about the author's viewpoints, what might be missing or wrong in trusted data stores [like from the government] or the wrangling and dead ends [*which one sometimes doesn't know are dead ends for the first few weeks*] that weren't included in the discussion to get this far! Semistructured data digestible like babyfood for computing does not ever just spring into existence -- *you have to be kinda careful about what you put in your baby's mouth, even if you swallow tasty crap Doritos or energy bars.*  In real life, you have be 1000X MORE careful about the data ... but this book is ONLY about playing with data analytix toys and misinterpretations you build on a petabyte scale.

*If you can see your path laid out in front of you step by step, you know it’s not your path. Your own path you make with every step you take. That’s why it’s your path.* -- Joseph Campbell

If you want your path to be on a solid footing -- go back and re-read the first three chapters ... or else you will be one of those immature souls who builds one of those annoying automated clusterfucks in which people are excited to get the crayons out of the box and draw a pretty picture for Grandma ... the rest of this is in "Knowing Just Enough To Build Something Dangerous" category. 

PLEASE be absolutely certain that you understand why you are reading this book and why you need to have semistructured data ... you will find little tricks or techniques or tactics that help you in the rest of the material ... but the first three chapters are the most important strategic part of this book.

4. Collecting and Displaying Records

The [Collecting and Displaying Records chapter](https://learning.oreilly.com/library/view/agile-data-science/9781491960103/ch04.html) discusses how you download flight data and then connect or “plumb” the flight records in that data through to a web application.

5. Visualizing Data with Charts and Tables

The [Visualizing Data with Charts and Tables chapter](https://learning.oreilly.com/library/view/agile-data-science/9781491960103/ch05.html) steps you through how to navigate your data by preparing simple charts in a web application.

6. Exploring Data with Reports

The [Exploring Data with Reports chapter](https://learning.oreilly.com/library/view/agile-data-science/9781491960103/ch06.html) teaches you how to extract entities from your data and parameterize and link between them to create interactive reports.

7. Making Predictions

The [Making Predictions chapter](https://learning.oreilly.com/library/view/agile-data-science/9781491960103/ch07.html) takes what you’ve done so far and predicts whether your flight will be on time or late.

8. Deploying Predictive Systems

The [Deploying Predictive Systems chapter](https://learning.oreilly.com/library/view/agile-data-science/9781491960103/ch08.html) shows how to deploy predictions to ensure they impact real people and systems.

9. Improving Predictions

The [Improving Predictions chapter](https://learning.oreilly.com/library/view/agile-data-science/9781491960103/ch09.html) illustrates how to iteratively improves on performance, using this particular example of on-time flight prediction.
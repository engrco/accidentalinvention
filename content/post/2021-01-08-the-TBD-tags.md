---
title: What's up with the TBD and RoughDraft tags
subtitle: To Be Done means various stages of not being ready
date: 2021-01-02
tags: ["intelligence", "genuine", "competent", "TBD0"]
---


TBD0 ... pure gas, wild-ass brainstorm, just kicked out there, not really edited or revised at all yet

TBD1 ... has had only one revision, probably just fixing gramamar or spelling

TBD2 ... has had second revision, MAYBE we might keep this idea to generate more ideas

TBD3 ... has had third revision, HOW could we fix this into something that might be worthwhile

TBD4 ... has had fourth revision, let's try to think more seriously about tightening this idea up

TBD5 ... has had fifth revision, after it sits for another month or so, maybe we then can revise it into #RoughDraft status.
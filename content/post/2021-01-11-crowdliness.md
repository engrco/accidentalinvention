---
title: Crowdliness
subtitle: Moving past "n of 1" personal health and OTHER forms of science; making it playable.
date: 2021-01-11
tags: ["intelligence", "TBD0"]
---


This is just a placeholder template an advertisment of the project that we are working on ... 86400.guru is about GTD and really USING all 86400 seconds in every day, ie even if we're napping, it should be and ENJOYED good nap, not a hiding-from-the-world not getting out of bed crappy nap ... time mgmt is an old topic that still has remarkably great ideas, but one of those ideas is to spend a few moments everyday, in some sort of *visionary* mode ... sketching out dreams ... ANOTHER one of those ideas is to spend a few moments of every day reflecting upon the performance in the last day, in some sort of *wrestling coach* mode.

The play.dot something SANDBOX is pretty much the consistent vision in our methodology ... this is just the idea of what should be the template for a post with different levels to it OR at least a general discussion of how we'd get to that interactivity ... it's not just about having ideas -- it's about setting the stage for readers to interact and play and develop a genuine shared intelligence ... we need to extend Hugo ... develop things in Figma, WebAssembly that allow for playability.

This started because we just became really curious [the robust, free tier of observability](https://grafana.com/?pg=play&plcmt=home) from [Grafana](https://grafana.com/grafana/) and the open source [Grafana project on Github](https://github.com/grafana/grafana) is cool and we are looking at emulating the [Grafana playground](https://play.grafana.org/d/000000012/grafana-play-home?search=open&orgId=1)
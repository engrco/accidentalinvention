---
title: META developments of the pre-prints
subtitle: arXiv, bioRxiv, medRxiv, chemRxiv, engRxiv
date: 2021-01-02
tags: ["intelligence", "genuine", "XIV0", "TBD0"]
---


This particular blog posting is some of the more leading edge project in knowledge engineering in the the [arXivLabs](https://labs.arxiv.org/) as well as an independent, unofficial collaboration using the [arXiv API Access](https://arxiv.org/help/api/) ... the developments in the pre-print server space are ... awe-inspiring and *encourage* speculation in terms of what kinds of learning might be possible in the realm of something we might call Agile Data Science 3.0 ... how do we add in development/writing/revision control/ CI/CD tools make DISTIBUTED open science a Reality?

## Connected Papers

Explore [connected papers](https://www.connectedpapers.com/) in a visual graph and try to keep up with [their release notes](https://www.notion.so/Connected-Papers-Release-Notes-f758d434dfcc45e2b8ed3d0452739982) ... *and while you are at it, you might want to set up your own [Notion.So](https://www.notion.so/Getting-Started-36bbf849cb954d3cb5aa3914eeaaec0d) account.*
* Get a visual overview of a new academic field ... just enter a typical paper and [connected papers](https://www.connectedpapers.com/) will build you a graph of similar papers in the field. Explore and build more graphs for interesting papers that you find - soon you'll have a real, visual understanding of the trends, popular works and dynamics of the field you're interested in.
* Create a bibliography to your thesis ... start with the references that you will definitely want in your bibliography and use [connected papers](https://www.connectedpapers.com/) to fill in the gaps and find the rest!
* Discover the most relevant prior and derivative works ... Use the [Prior Works button](https://www.connectedpapers.com/main/ac0950da3570eb6a77991ca3ebcf9007c3543151/Quantum-Technologies-A-Review-of-the-Patent-Landscape/prior) to find important ancestor works in your field of interest. Use the [Derivative Works button](https://www.connectedpapers.com/main/ac0950da3570eb6a77991ca3ebcf9007c3543151/Quantum-Technologies-A-Review-of-the-Patent-Landscape/derivative) to find literature reviews of the field, as well as recently published State of the Art that followed your input paper.
* Or simply make sure that have not missed an important paper by exploring paper-space ... In some fields like Machine Learning, so many new papers are published it's hard to keep track. With [connected papers](https://www.connectedpapers.com/) you can just search for keywords and then visually discover important recent papers and then follow your paper-space bread-crumbs by looking back at "My graphs" No need to keep lists.

## Papers With Code

arXiv Links to Code aims to provide an easy and convenient way to find relevant code for a paper ... using data from [Papers with Code](https://paperswithcode.com/) - a free resource that links trending papers with code repositories, including the [latest](https://paperswithcode.com/latest), [greatest](https://paperswithcode.com/greatest), [state-of-the-art](https://paperswithcode.com/sota) [datasets](https://paperswithcode.com/datasets), the [libraries](https://paperswithcode.com/libraries) and [methods](https://paperswithcode.com/methods) being used, [trends](https://paperswithcode.com/trends) in Machine Learning. Papers with Code is the biggest such resource and exploring it fully is ...  sort of unfathomable. Of course, it is licensed under an open license *so nobody has any excuse for not using it*.

## CORE Recommender

Explore relevant open access papers from across a global network of research repositories while browsing arXiv. Research papers are recommended from both arXiv and other over 10 thousand open access data providers and brought to you by [CORE](https://core.ac.uk/), the world’s largest aggregator of open access research.

## arXiv Bibliographic Explorer

[Matt Bierbarm](https://github.com/mattbierbaum)'s [arXiv Bibliographic Explorer](https://github.com/mattbierbaum/arxiv-bib-overlay) is an amazingly useful open source javascript package (bookmarklet and chrome [firefox] extension), which displays the results of Semantic Scholar, ADS, Inspire HEP API calls onto arXiv abstract pages. The bookmarklet can be [added to the browser](https://mattbierbaum.github.io/arxiv-bib-overlay/static/downloads.html) to display information about works that cite and are cited by arXiv papers and their published versions. The primary objective of the project is to enable discovery of relevant research and context by providing user-friendly navigation of an article's citation tree.

{NOTE: Of course, this posting obviously should eventually apply to the [bioRxiv](https://www.biorxiv.org/), [medRxiv](https://www.medrxiv.org/), [chemRxiv](https://www.medrxiv.org/), [engRxiv](https://www.engrxiv.org/) PRE-PRINT servers for Biology, Medicine, Chemistry and Engineering, respectively ... it's just that THIS particular post happens to be a meta-Rxiv post about the collaborative knowledge engineering projects for all pre-print servers.}


---
title: Dogfood the Git in Gitlab
subtitle: Dogfood in order to better learn your tools/craft
date: 2021-02-09
tags: ["intelligence", "genuine", "TBD0"]
---

It is up to you to make your tools better ... but having said that, we probably could not be bigger fans of Gitlab.  We want to engage with the GitLab community and be a contributor to the outstanding open source project that is Gitlab, right down to the roots of Git. 

There are IMPORTANT questions to really understand before we get going with Git ... for example, why does your version control system NEED to be DISTRIBUTED? 

If you don't understand the answer to this question ... at the gut level, without thinking about it ... you will never have the energy to learn about Git, because mastering Git can be kind of HARD.  Not everybody has to use Git ... maybe you're the type of person who is like my parents or older relatives who hate computers and should just try to use Windows File Explorer without screwing it up ... or, maybe you are one baby step above that and you just want to use OneDrive or Dropbox or Google Drive -- that's fine ... but EVERYTHING about Accidental Invention is going to be based upon your literacy with Git ... that's how core Git is to our dogfood.
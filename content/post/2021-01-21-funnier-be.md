---
title: Funnier.Be humor dissection
subtitle: Not just Bernie memes ... but crowdsourced, data-driven and playable
date: 2021-01-21
tags: ["intelligence", "TBD0"]
---


WHY is the Bernie meme so funny to people?  What is it about that iconic image? Of course, the mittens meme is hardly going to be last time something like this happens ... remember the "didn't hang himself" meme, for example?   

This is just a placeholder template ... the play.dot something SANDBOX is the vision ... this is just the idea of what should be the template for a post with different levels to it OR at least a general discussion of how we'd get to that interactivity ... it's not about having ideas -- it's about setting the stage for readers to interact and play and develop a genuine shared intelligence. 

... because, after playing with dashboards, we just became really curious about [the robust, free tier of observability](https://grafana.com/?pg=play&plcmt=home) from [Grafana](https://grafana.com/grafana/) and the open source [Grafana project on Github](https://github.com/grafana/grafana) is cool and we are looking at emulating the [Grafana playground](https://play.grafana.org/d/000000012/grafana-play-home?search=open&orgId=1)
---
title: What's up with the OBADDD tags
subtitle: One Book A Day Deep Dive
date: 2021-01-02
tags: ["intelligence", "genuine", "OBADDD0", "TBD0"]
---


We read ... write a comment ... put the book down ... come back to it ... so we tag a post with the One Book A Day Deep Dive or OBADDD-prefix ... the last digit is an indicator or how many times we have come back to book 

OBADDD0 ... just picked up, initial flip-thru reaction, not really edited or revised at all yet

OBADDD1 ... has had only one revision, probably just fixing gramamar or spelling

OBADDD2 ... has had second revision, MAYBE we might keep this idea to generate more ideas

OBADDD3 ... has had third revision, HOW could we fix this into something that might be worthwhile

OBADDD4 ... has had fourth revision, let's try to think more seriously about tightening this idea up

OBADDD5 ... has had fifth revision, after it sits for another month or so, maybe we then can revise it into #RoughDraft status.
---
title: Wear The Mask
subtitle: When The Well-Informed Stop Thinking
date: 2021-03-08
tags: ["anti-intelligence", "genuine", "TBD0"]
---

"Avoid doing THAT which makes you stupider" ... and you are well equipped to understand what THAT is, but sometimes we just gotta say, *"C'mon, man."*

The [BEST strategy that lowered covid mortality and minimized other mortality, morbidity and misery](https://twitter.com/ClareCraigPath/status/1369219568762318851) was always about some flavor of freedom, sharing of ridiculous amounts of information and then trusting intelligent people to BECOME REALLY, REALLY INTELLIGENT because the lives of people they cared about were on the line. 

The alternatives were NEVER just total lockdown or "Neanderthal thinking" ... the lockdown Maski path that was chosen was the path to CERTAIN extinction if humans don't learn from it ... but, of course, BILLIONS of people have learned and they might not yet know exactly what to do about it, but they have learned that the Maski lies are entirely fabricated BULLSHIT by people who imagine that they are smarter than the billions [and they clearly aren't.]

Trusting ANY politicians or ANY news media organizations WILL make you stupider ... the evidence abounds that the world is now packed by people who born intelligent and then, through the magic of the educational system, constant attention to news media to be well-informed and trusting their *elected?* *leaders??*, people have become ridiculously less than shrewd and STUPID.

[People in shorts, with their masks but no helmet on, riding their motorcycle down a highway at a fairly high rate of speed](https://twitter.com/RWNutjob1/status/1368897133655494656) may illustrate why no amount of information ... including all of the solid data from a variety of verifiable sources OR all peer-reviewed papers in the world of science ... ever going to convince fully-hypnotized, well-informed, educated mask wearers to actually go to the trouble of rationally evaluating and ameliorating risks.

Virtue signallers NEED to signal virtue ... virtue is more of a "big hat, no cattle" kind of thing than the actual "big hat, no cattle" kind of thing.

There are VERY RARE exceptions ... but almost without exception ... professional, well-educated people who consider themselves to be well-informed because they read newspapers/magazines / blogs [in paper or electronically] and listen to their trusted news programming / content channels every day simply DO NOT THINK any more.
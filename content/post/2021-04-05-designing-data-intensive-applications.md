---
title: Designing Data-Intensive Applications Science 2.0    
subtitle: A One Book A Day Deep Dive
date: 2021-04-05
tags: ["intelligence", "genuine", "OBADDD", "TBD0"]
---


There is a TWO-FOLD rationale for looking into a book like [Designing Data-Intensive Applications](https://learning.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/) it's not JUST a really important work in the sub-genre of excellent technical books devoted to the the [Data Engineering](https://learning.oreilly.com/topics/data-engineering/) topic ... that alone would be sufficent ... but *there's more* -- Martin Kleppmann's writing is also [one of Tim O'Reilly's examples of a technical book that is ***a super clear read.***](https://learning.oreilly.com/videos/expert-playlist-intro/0636920319801/0636920319801-video326369) -- technology might change over time [although most things about the general architecture of data-intensive applications might be kind of quasi-timeless, even if the specifics change], but it does not take long, as one reads what Martin Kleppman has written that one thinks, "*That's pretty good -- I am going to try to write more like that.*"

1. [Reliable, Scalable, and Maintainable Applications](https://learning.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/ch01.html)

Many applications today are data-intensive, as opposed to compute-intensive. Raw CPU power israrely a limiting factor for these applications—bigger problems are usually the amount of data,the complexity of data, and the speed at which it is changing.


2. [Data Models and Query Languages](https://learning.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/ch02.html)

Data models and programming languages [including query languages] are perhaps the most important part of developing software because they have such a profound effect how we even BEGIN begin to formulate ideas and think about the problem that we are solving.  The limits of languages and data models limit the realm of software ... in good ways, perhaps, but also in ways that we don't even realize because we don't have a way to even express what is missing.

3. [Storage and Retrieval](https://learning.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/ch03.html)

A database needs to do ONLY two things. When you give it some data, it should store that data. When you ask it again later, it should give the same data back to you. This is not a trivial task given the insecure, fault-prone internet environment in which the database must operate and the number to times that your applications will ask it to perform these two things.

4. [Encoding and Evolution](https://learning.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/ch04.html)

Applications MUST inevitably change, but change in some sort of predictable compatible way over time. Features are added or modified as new products are launched, user requirements change and become worse or hopefully better understood and business circumstances change in a completely unpredictable manner ... the user does not care about any of that upgrade or change stuff, unless they requested a feature that they think the application should already have and that feature never shows up.

5. [Replication](https://learning.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/ch05.html)

Replication means keeping a copy of the same data on multiple machines that are connected via a network ... geographically to reduce latency ... redundantly to provide fault-tolerance ... scalably to handle increase user participation and demands for throughput.

6. [Partitioning](https://learning.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/ch06.html)

For very large datasets, or very high query throughput, that is not sufficient: we need to break the data up into partitions, also known as sharding.

7. [Transactions](https://learning.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/ch07.html)

In the harsh reality of data systems, many things can go wrong ... the user does not care about any of that and is completely impatient and unforgiving about it; users only care that your application never threatens the integrity of THEIR transactions.

8. [The Trouble with Distributed Systems](https://learning.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/ch08.html)
Anything that can go wrong must go wrong -- it's necessary to have your shit tight, because you must also assume that you don't yet realize all of the combinatorics of everything in the environment in which system runs ... so it's as if stuff [you thought handled by others beyond your control] that wasn't supposed to be able to go wrong will go wrong. At the worst possible time.  

9. [Consistency and Consensus](https://learning.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/ch09.html)

The simplest way of handling catastrophic faults is to simply let the entire service fail, and show the user an error message. If that solution is unacceptable, we need to find ways of tolerating faults—that is, of keeping the service functioning correctly, even if some internal component is faulty. This chapter explores SOME examples of algorithms and protocols for building fault-tolerant distributed systems.

10. [Batch Processing](https://learning.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/ch10.html)

A system cannot be successful if it is too strongly influenced by a single person. Once the initial design is complete and fairly robust, the real test begins as people with many different viewpoints undertake their own experiments.  In online systems, eg, a web browser/app requesting a page or a service calling a remote API, the request is typically assumed to have been triggered by a human user and the user is waiting for the response. The web browser/app, and increasing numbers of HTTP/REST-based APIs, have made the request/response style of interaction so common that it’s easy to take it for granted, but there are other approaches have their merits ... online services ... offline batch processing ... streaming near-real-time systems.  It might seem *old*, but batch processing is still an extremely important building block the overall desire for reliable, scalable, and maintainable applications ... but more and better data, eg video/audio ... and that also means more and better uses for data, ie, not *old* as much as permanent and still changine and growing.

11. [Stream Processing](https://learning.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/ch11.html)

In reality, a lot of data is unbounded because it arrives gradually over time: your users produced some data yesterday and more today, and they will continue to produce much more data tomorrow. Unless you go out of business, this process never ends, and so the dataset is never “complete” in any meaningful way. In general, a “stream” refers to data that is incrementally made available over time. This chapter looks at event streams, eg audio, video, and  how they are treat as a data management mechanism: the unbounded, incrementally processed counterpart to the batch data we saw in the last chapter. How streams are represented, stored, and transmitted over a network is discussed first. Then, the relationship between streams and databases is investigated. And finally, approaches and tools for processing those streams continually, and ways that they can be used to build applications are explored.

12. [The Future of Data Systems](https://learning.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/ch12.html)
This is not a prediction, but a more of a discussion or how things should be ... fortunately, there are a lot of readers of this influential book ... and through their impact, there's a good chancet that what should happen will happen. 
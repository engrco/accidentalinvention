---
title: Distributed Open Science
subtitle: Agile Data Science 3.0
date: 2021-03-13
tags: ["intelligence", "genuine", "XIV0", "TBD0"]
---


It's not really sufficient for knowledge engineering operations to be focused in hubs or centers.  The hub or center is not about "excellence" ... it is about CONTROL ... if we want REAL Science, the scientific method must be followed by FREE minds in DISTRIBUTED virtual communities.  So of course, we are excited by [arXivLabs](https://labs.arxiv.org/) projects, but we might be even more excited about the independent, unofficial collaborations which use, among other things, the [arXiv API Access](https://arxiv.org/help/api/) ... our goals is something that something we might call Agile Data Science 3.0 ... how do we enable development/writing/revision control/ CI/CD tools to make HIGHLY DISTIBUTED open science a reality?

If one thing is clear ... if we have learned one thing from the life of [Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz), it is necessary to move past the gatekeepers, past Closed Science.  

The contrast is stark ... although Open Science, still depends upon the scientific method -- Science should not be constrained to institutions; we know from the history of Science and the advances made by independent scientists unaffliated with any lab, Science must be FREE ... scientists must be able [financially, economically] to choose, in the free marketplace of work alternatives, whether to work inside an institution OR to work independently. That will involve developing ways to learn or develop knowledge PRACTICALLY from the experiences of crowds of FREE "n of 1" individuals ... even though the implications of that will be mind-blowing for calcified, institutionalized academic and government scientists, it will ultimately produce a Science that transends what can be accomplished under the currently constrained institutional Closed Science.


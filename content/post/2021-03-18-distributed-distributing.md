---
title: Distributed Distributing
subtitle: Distributing Distributed Distributing
date: 2021-03-13
tags: ["intelligence", "genuine", "XIV0", "100books", "TBD0"]
---


The plenty of opportunity to contribute BY distributing comments, insights, gists, wikia and INTERACTING with the community who share some sort of appreciation for something such as one of your 100 favorite books, for instance... https://godel-escher-bach.fandom.com/wiki/G%C3%B6del,_Escher,_Bach_Wiki

It is always difficult to think and reason in a new language and that's a GOOD thing. Difficulty makes us stronger.  NO HUMAN EVER has been perfect at thinking or reasoning in their language; that's WHY it's important to have conversations, interactions, even arguments with people, in order to get some insight into how other people think, how they reason, how they are DIFFERENT.

DIFFERENT is good. Distribute the DIFFERENT. Encourage the distribution of diversity, rather than ethnic cleansing.

Distibuted thinking is about processing information ON DIFFERENT PROCESSORS.  This why OPEN Distributed Science done by DIFFERENT thinkers will ultimately produce a Science that transends what can be accomplished under the currently constrained, unified monolith of Authoritarian institutional Closed Science.


---
title: The public transportation metric
subtitle: How you can tell that most automobile drivers are mindless about it
date: 2021-03-12
tags: ["intelligence", "TBD0"]
---


As libertarians, we perhaps should not like public transportation as much as we do ... EXCEPT that you can tell that most automobile drivers do not make rational fully considered choices about driving; in fact, you can tell that MOST automobile drivers hate public transportation because getting on a bus or subway forces them to actually THINK about their trip.Automobile drivers tend to be completely mindless about driving their automobiles in this regard ... in a way that is extremely different from a truck driver, driver of delivery van or driver of any kind of company vehicle where GPS location, speed, acceleration, decceleration, fuel consumption and other attributes of safe driving are tracked. Moreover, automobile drivers tend to believe that taxpayers should cough up extra money when gas taxes prove to be insufficient for roadway maintenace and construction -- automobile drivers tend to have a VERY anti-libertarian religous belief that roads benefit everyone.

So, as good libertarians, we tend to love public transportation ... in part, because we KNOW that there will be intense resistance and more intense scrutiny of any public transportation project ... in fact, most people are more likely to support massive rebates for electric vehicles or something even more insane, ie the renewable fuel standard.

So let's just leave it there ... we can live without ANY funding for transportation, okay. Survivalist can walk.  If your candy-ass lifestyle requires someone else to support it ... WTF ...c'mon, man.
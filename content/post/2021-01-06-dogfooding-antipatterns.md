---
title: Dogfood Your Process; Work Git Into Everything
subtitle: Internalize Git until GitThink is the ubiquitous, unexamined mindset
date: 2021-01-06
tags: ["intelligence", "genuine", "TBD0"]
---


The BIGGEST anti-practice in all of software ... including even dogfooding is get excited about the tools, the gear, the concepts ... but to never USE something to the point where you cannot even begin to think without USING it.

For example, you should THINK in terms of DISTRIBUTED version control systems ... dogfooding is not about getting enamored with features or concepts -- it's about USING, iterating, then USING the refinement or maybe rolling back to what works and trying a new rev ... but USING, USING, USING ... the antipattern is something like *"Wow, yeah, I totally get it, that'd be so kewl, I know to do it, I will release the thing that saves humanity ... it crashes ... blame the users for doing it wrong."*

**HINT: Remove the language/equivalentspeak of *"You're doing it wrong!*" from your vocabulary ... UNLESS you're just making fun of people who use that language.**

Use Git in your toolchain for as much of your work as possible -- read and steal/plagiarize/dogfood your own internalized copy most of the practices from [how the best companies, like Gitlab, use Git or even Gitlab Pages](https://about.gitlab.com/handbook/product/product-processes). 

Hide NOTHING. Expect others to be familiar with the process and to be able to look in the place where things are supposed to be. Be consistent in aggressively dogfooding ... but try to make every commit, every code comment, ever error message USEFUL ... so that it's not necessary to explain later. Try to NEVER not to use external tools or proprietary tools. For example, Use Git Pull Requests and Issues instead of first resorting to an email, an attachment, Slack channel, Signal, social media or any kind of method that does not tie information into Git or a Git Commit.

# Git Sprintology

Set-up something like a [four-week agile code sprint](https://learning.oreilly.com/library/view/the-art-of/9781492080688/ch03.html) in order to get the LEAN MVP of your own git workflow into your toolchains and automated workflow ... but the real point of git is to ALWAYS BE DEPLOYING ... and monitoring ... and adjusting ... and planning the next sprint.
---
title: BASIC Continuous Integration
subtitle: With some practical chaos engineering simulating I/O faults at runtime
date: 2021-01-10
tags: ["intelligence", "OABDDD0", "TBD0"]
---

We are only starting to put this together -- so let's go back and look the basics of the basics ... for groundwork, let's look at the important fundamentals of [engineering systems integration](https://learning.oreilly.com/library/view/engineering-systems-integration/9781439852897/).  This not just a software thing -- these fundamentals are important in the engineering of all kinds of systems.

## Principle 1: The Principle of Alignment

Alignment of strategies for the business enterprise, the key stakeholders, and the project results in better outcomes for product or service development.

## Principle 2: The Principle of Partitioning

Partitioning of objects can create tractable problems to solve if and only if boundary contiguity is achieved.

## Principle 3: The Principle of Induction

We play with things in order to induce failures and opportunities, because there are things we miss when we just deduce or reason based on assumptions -- inductive reasoning with chaos engineering should guide integration management and recursive thinking.

## Principle 4: The Principle of Limitation

Integration is only as good as the architecture of the test environment that fully captures stakeholder requirements. The test rig is never as good as the real world; we will ALWAYS be improving the test rig to better match the real world. Nobody gets to say to the customer, "But, you're doing it wrong ... waaaaah."  The product must be re-designed or the failure mode somehow elimated so that mere warnings are no longer necessary OR the requirements and test rig has to be modified.

##  Principle 5: The Principle of Forethought

Integration must be a primary, key pre-cursor activity, done up front and integrated into development -- not an afterthought considered as the result of development or the step after development.

## Principle 6: The Principle of Planning

Integration planning is predicated on pattern scheduling (lowest impact on budget), network scheduling (determinable impact on budget), and ad hoc scheduling (undetermined impact on budget).

## Principle 7: The Principle of Loss

When two objects are integrated, both objects give up some measure of autonomous behavior ... this is not always necessarily a loss, ie integrated objects may deliver far more positives, but we must understand that the objects are now linked or constrained in some way.

# Doing CI / CD PRACTICALLY

NOW let's think about the more specific example of [CI or Software Quality](https://learning.oreilly.com/topics/continuous-integration/) in [DevOps](https://learning.oreilly.com/topics/devops/)  ... let's say that we have somehow grokked most of the [DevOps Essentials](https://learning.oreilly.com/playlists/cea7c71b-6816-410d-b667-2014be8b9127/) ... and, as a result, we are just zealously, evangelically committed to spreading the gospel of serious test driven development ... *we believe that people writing the code really have to eat their own cooking* ... so that translates into how we really want to break things early AND break things OFTEN and stochastically ... so stochastically really often = simulated often ... so we are gonna need a CI/CD toolchain or apparatus-of-apparatuses for that ... for a LOCAL dev toolchain.  

That means we might be using or adapting something like Kind. Kind is a tool for running local Kubernetes clusters using Docker container "nodes." Kind WAS originally designed for testing Kubernetes, ie just Kubernetes itself ... but Kind can be used for local development OR continuous integration OR extended for new uses yet to be defined ... by the time we get our rig defined, it might be have a few scars and some history -- so it might not be so Kind anymore -- but, for right now, Kind is a good starting point.

To simulate I/O faults at runtime, we need to inject faults into a filesystem after the program starts system calls (such as reads and writes) but before the call requests arrive at the target filesystem. We could do that with Berkeley Packet Filter (BPF); however, BPF cannot really be used to inject delay so we add a filesystem layer with Chaos Mesh, called ChaosFS before the target filesystem. ChaosFS uses the target filesystem as the backend and receives requests from the operating system. The entire call link is target program syscall -> Linux kernel -> ChaosFS -> target filesystem. Because ChaosFS is customizable, we can inject delays and errors as we want and we can KIND of play with the stochasticisty of those injections. So ChaosFS plus Kind plus our own monkeybusiness is our choice.